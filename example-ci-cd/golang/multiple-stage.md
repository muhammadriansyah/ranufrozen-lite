FROM golang:alpine as builder

WORKDIR /app 

COPY go.mod .

COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" .

FROM scratch

WORKDIR /app
EXPOSE 8000
COPY --from=builder /app/ranufrozen-lite /usr/bin/

ENTRYPOINT ["ranufrozen-lite"]
