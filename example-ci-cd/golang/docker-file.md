hasil building image

REPOSITORY                                      TAG       IMAGE ID       CREATED         SIZE
<none>                                          <none>    c33b1ca1f9db   5 seconds ago   373MB
ranufrozen-lite                                 latest    4742953560f1   5 seconds ago   4.46MB

hanya 4 MB dengan multi-stage!
Transfer nya harus nya lebih ringan

jangan lupa expose port

```Dockerfile
FROM golang:alpine as builder

WORKDIR /app 

COPY go.mod .

COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" .

FROM scratch

WORKDIR /app
EXPOSE 8000
COPY --from=builder /app/ranufrozen-lite /usr/bin/

ENTRYPOINT ["ranufrozen-lite"]

```