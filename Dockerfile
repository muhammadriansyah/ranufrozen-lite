FROM golang:alpine

WORKDIR /app 

COPY go.mod .

COPY go.sum .

RUN go mod download

COPY . .

EXPOSE 8000

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" .

ENTRYPOINT ["ranufrozen-lite"]
