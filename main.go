package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"ranufrozen-lite/food"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v9"
	"github.com/joho/godotenv"
)

func main() {
	errDotenv := godotenv.Load()
	if errDotenv != nil {
		log.Fatal("Error loading .env file")
	}

	rcl := GetRedisConn()
	foodRepo := food.NewRepository(rcl)
	foodRepo.SeedData()
	startAPI(foodRepo)
}
func startAPI(foodRepo *food.Repository) {
	router := gin.Default()

	router.GET("/", func(ctx *gin.Context) {
		ctx.JSON(200, gin.H{
			"data": os.Getenv("REDIS_PASSWORD"),
		})
	})

	router.GET("/foods", func(ctx *gin.Context) {
		ctx.JSON(200, gin.H{
			"data": foodRepo.GetFoods(),
		})
	})

	router.Run(":8000")
}

func GetRedisConn() *redis.Client {
	address := fmt.Sprintf("%v:6379", os.Getenv("REDIS_HOST"))
	log.Println(address)
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: "",
		DB:       0,
	})
	err := client.Ping(context.TODO()).Err()
	if err != nil {
		log.Println(err)
		log.Println("forget to running redis-server")
		fmt.Printf("redis-server --save 20 1 --loglevel warning --requirepass %s --daemonize yes\n", os.Getenv("REDIS_PASSWORD"))
	}
	return client
}
