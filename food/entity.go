package food

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/go-redis/redis/v9"
)

type Food struct {
	ID    int
	Name  string
	Price float64
}

var ctx = context.TODO()

type Repository struct {
	rcl *redis.Client
}

func NewRepository(rcl *redis.Client) *Repository {
	return &Repository{rcl}
}

func (rep Repository) GetFoods() []Food {
	foodJson, err := rep.rcl.Get(ctx, "foods").Bytes()
	if err != nil {
		log.Println(err)
	}

	foodArr := []Food{}
	err = json.Unmarshal(foodJson, &foodArr)

	if err != nil {
		log.Println(err)
	}

	return foodArr
}

func (rep *Repository) SeedData() {
	foodJson := []byte("[{\"ID\":1,\"Name\":\"Caprese Salad\",\"PhotoPath\":\"breakfast_item.jpg\",\"Rating\":1,\"Price\":53191,\"Stock\":20,\"IsSuperSeller\":1,\"Category\":3,\"QuantitySold\":65,\"Description\":\"Rasanya bikin nyam nyam\",\"Discount\":0.1,\"CreatedAt\":\"0001-01-01T00:00:00Z\",\"UpdateAt\":\"0001-01-01T00:00:00Z\"},{\"ID\":2,\"Name\":\"Stinky Tofu\",\"PhotoPath\":\"breakfast_item.jpg\",\"Rating\":4,\"Price\":42233,\"Stock\":1,\"IsSuperSeller\":1,\"Category\":4,\"QuantitySold\":49,\"Description\":\"Rasanya bikin nyam nyam\",\"Discount\":0.1,\"CreatedAt\":\"0001-01-01T00:00:00Z\",\"UpdateAt\":\"0001-01-01T00:00:00Z\"},{\"ID\":3,\"Name\":\"Stinky Tofu\",\"PhotoPath\":\"breakfast_item.jpg\",\"Rating\":3,\"Price\":36535,\"Stock\":16,\"IsSuperSeller\":0,\"Category\":4,\"QuantitySold\":92,\"Description\":\"Rasanya bikin nyam nyam\",\"Discount\":0.1,\"CreatedAt\":\"0001-01-01T00:00:00Z\",\"UpdateAt\":\"0001-01-01T00:00:00Z\"},{\"ID\":4,\"Name\":\"Som Tam\",\"PhotoPath\":\"breakfast_item.jpg\",\"Rating\":1,\"Price\":38002,\"Stock\":17,\"IsSuperSeller\":0,\"Category\":2,\"QuantitySold\":91,\"Description\":\"Rasanya bikin nyam nyam\",\"Discount\":0.1,\"CreatedAt\":\"0001-01-01T00:00:00Z\",\"UpdateAt\":\"0001-01-01T00:00:00Z\"},{\"ID\":5,\"Name\":\"California Maki\",\"PhotoPath\":\"breakfast_item.jpg\",\"Rating\":4,\"Price\":44209,\"Stock\":4,\"IsSuperSeller\":1,\"Category\":5,\"QuantitySold\":76,\"Description\":\"Rasanya bikin nyam nyam\",\"Discount\":0.1,\"CreatedAt\":\"0001-01-01T00:00:00Z\",\"UpdateAt\":\"0001-01-01T00:00:00Z\"},{\"ID\":6,\"Name\":\"Pasta and Beans\",\"PhotoPath\":\"breakfast_item.jpg\",\"Rating\":3,\"Price\":52071,\"Stock\":9,\"IsSuperSeller\":1,\"Category\":2,\"QuantitySold\":62,\"Description\":\"Rasanya bikin nyam nyam\",\"Discount\":0.1,\"CreatedAt\":\"0001-01-01T00:00:00Z\",\"UpdateAt\":\"0001-01-01T00:00:00Z\"},{\"ID\":7,\"Name\":\"Sushi\",\"PhotoPath\":\"breakfast_item.jpg\",\"Rating\":4,\"Price\":48103,\"Stock\":17,\"IsSuperSeller\":0,\"Category\":3,\"QuantitySold\":16,\"Description\":\"Rasanya bikin nyam nyam\",\"Discount\":0.1,\"CreatedAt\":\"0001-01-01T00:00:00Z\",\"UpdateAt\":\"0001-01-01T00:00:00Z\"},{\"ID\":8,\"Name\":\"Meatballs with Sauce\",\"PhotoPath\":\"breakfast_item.jpg\",\"Rating\":4,\"Price\":33030,\"Stock\":2,\"IsSuperSeller\":0,\"Category\":3,\"QuantitySold\":93,\"Description\":\"Rasanya bikin nyam nyam\",\"Discount\":0.1,\"CreatedAt\":\"0001-01-01T00:00:00Z\",\"UpdateAt\":\"0001-01-01T00:00:00Z\"}]")
	err := rep.rcl.Set(ctx, "foods", foodJson, 1*time.Hour).Err()
	if err != nil {
		log.Println(err)
	} else {
		fmt.Println("success seed data")
	}

}
